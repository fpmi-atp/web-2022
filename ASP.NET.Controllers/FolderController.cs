using ASP.NET.Controllers.Dto;
using ASP.NET.Service;
using ASP.NET.Service.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ASP.NET.Controllers;

[ApiController]
[Route("[controller]")]
public class FolderController : ControllerBase
{
    private readonly ILogger<ProductController> _logger;
    private readonly IFolderService _folderService;

    public FolderController(ILogger<ProductController> logger, IFolderService folderService)
    {
        _logger = logger;
        _folderService = folderService;
    }

    [HttpPost]
    public async Task Add(string title, long? parentFolderId)
    {
        await _folderService.Add(title, parentFolderId);
    }

    [HttpDelete]
    public async Task Delete(long folderId)
    {
        await _folderService.Delete(folderId);
    }

    [HttpGet]
    public async Task<IEnumerable<FolderOutputDto>> Get()
    {
        return await _folderService.GetAll();
    }
}