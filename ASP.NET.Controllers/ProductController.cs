using ASP.NET.Controllers.Dto;
using ASP.NET.Service;
using ASP.NET.Service.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ASP.NET.Controllers;

[ApiController]
[Route("[controller]")]
public class ProductController : ControllerBase
{
    private readonly ILogger<ProductController> _logger;
    private readonly IProductService _productService; // TDD

    public ProductController(ILogger<ProductController> logger, IProductService productService)
    {
        _logger = logger;
        _productService = productService;
    }

    [HttpGet]
    public async Task<IEnumerable<ProductListItemOutputDto>> Get(
        [FromQuery]string? searchString,
        [FromQuery]int? limit,
        [FromQuery]int? offset,
        [FromQuery]bool? orderByAsc,
        CancellationToken cancellationToken)
    {
        var products = await _productService
            .Get(searchString, limit, offset, orderByAsc, cancellationToken);

        return products
            .Select(product => new ProductListItemOutputDto
            {
                Id = product.Id,
                Title = product.Title
            })
            .ToList();
    }

    [HttpGet("product/{productId:long}")]
    public async Task<DetailedProductOutputDto> Get([FromRoute] long productId, CancellationToken cancellationToken)
    {
        var product = await _productService.Get(productId, cancellationToken);

        return new DetailedProductOutputDto
        {
            Id = product.Id,
            Title = product.Title,
            CategoryId = product.CategoryId,
            CategoryTitle = product.CategoryTitle
        };
    }

    [HttpPost]
    public async Task<OutputProductDtoNew> Add([FromQuery]InputProductDto inputProduct, CancellationToken cancellationToken)
    {
        return await _productService.Add(inputProduct, cancellationToken);
    }

    [HttpPatch]
    public async Task<OutputProductDto> Update(
        [FromQuery]long productId,
        [FromBody]InputProductDto inputProduct,
        CancellationToken cancellationToken)
    {
        return await _productService.Update(productId, inputProduct, cancellationToken);
    }

    [HttpDelete]
    public async Task<OutputProductDto> Delete(
        [FromQuery]long productId,
        CancellationToken cancellationToken)
    {
        return await _productService.Delete(productId, cancellationToken);
    }

    [HttpGet("LongExecution")]
    public async Task<int> LongExecution(CancellationToken cancellationToken)
    {
        Console.WriteLine("BEFORE");

        cancellationToken.Register(() =>
        {
            Console.WriteLine("Cancelled");
        });

        await Task.Delay(4000, cancellationToken);

        Console.WriteLine("AFTER");

        return 10;
    }
}