﻿namespace ASP.NET.Controllers.Dto;

public class DetailedProductOutputDto
{
    public long Id { get; set; }

    public string Title { get; set; }

    public long CategoryId { get; set; }

    public string CategoryTitle { get; set; }
}