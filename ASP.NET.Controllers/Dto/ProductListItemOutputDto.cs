﻿namespace ASP.NET.Controllers.Dto;

public class ProductListItemOutputDto
{
    public long Id { get; set; }

    public string Title { get; set; }
}