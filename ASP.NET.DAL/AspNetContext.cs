﻿using ASP.NET.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace ASP.NET.DAL;

public class AspNetContext : DbContext
{
    public virtual DbSet<Product> Products { get; set; }

    public virtual DbSet<Category> Categories { get; set; }

    public virtual DbSet<Folder> Folders { get; set; }

    public AspNetContext(DbContextOptions<AspNetContext> options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Product>().HasQueryFilter(product => !product.IsDeleted);

        base.OnModelCreating(modelBuilder);
    }

    public override int SaveChanges(bool acceptAllChangesOnSuccess)
    {
        var products = this
            .ChangeTracker
            .Entries<Product>()
            .Where(product => product.State == EntityState.Modified)
            .ToList();

        foreach (var product in products)
        {
            product.Entity.UpdatedAt = DateTime.UtcNow;
        }

        return base.SaveChanges(acceptAllChangesOnSuccess);
    }
}