﻿using ASP.NET.DAL.Repositories;
using ASP.NET.Service.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace ASP.NET.DAL.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddStorage(this IServiceCollection serviceCollection)
    {
        return serviceCollection
            .AddTransient<IRepository, Repository>()
            .AddTransient<IFolderRepository, FolderRepository>()
            .AddDbContext<AspNetContext>(options =>
            {
                options.UseSqlite("Data Source=demo.db");
            });
    }
}