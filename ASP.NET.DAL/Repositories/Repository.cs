﻿using ASP.NET.DAL.Entities;
using ASP.NET.Service;
using ASP.NET.Service.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace ASP.NET.DAL.Repositories;

public class Repository : IRepository
{
    private AspNetContext _dbContext;

    public Repository(AspNetContext context)
    {
        _dbContext = context;
    }

    public async Task<IReadOnlyList<OutputProductDtoNew>> GetAll(string? searchString, int? limit, int? offset)
    {
        var result = _dbContext
            .Products
            .AsNoTracking()
            .AsQueryable();

        if (searchString is not null)
        {
            result = result.Where(product => product.Title.Contains(searchString));
        }

        if (offset > 0)
        {
            result = result.Skip(offset.Value);
        }

        if (limit > 0)
        {
            result = result.Take(limit.Value);
        }

        var before = _dbContext.ChangeTracker.Entries<Product>().ToList();

        var products = await result
            .Select(product => new OutputProductDtoNew(product.Id, product.Title, product.CategoryId, product.Category.Title))
            .ToArrayAsync();

        var after = _dbContext.ChangeTracker.Entries<Product>().ToList();

        return products;
    }

    public Task<bool> HasProduct(string title, long categoryId)
    {
        return _dbContext
            .Products
            .Where(product => product.CategoryId == categoryId)
            .AnyAsync(product => product.Title == title);
    }

    public async Task<OutputProductDtoNew> CreateProduct(string title, long categoryId)
    {
        var newProduct = new Product
        {
            Title = title,
            CategoryId = categoryId
        };

        _dbContext.Products.Add(newProduct);

        await _dbContext.SaveChangesAsync();

        var category = await _dbContext
            .Categories.FindAsync(categoryId);

        if (category is null)
        {
            throw new Exception();
        }

        return new OutputProductDtoNew(newProduct.Id, newProduct.Title, category.Id, category.Title);
    }
}