﻿using ASP.NET.Service;
using ASP.NET.Service.Abstractions;
using Microsoft.EntityFrameworkCore;
using Folder = ASP.NET.Service.Folder;

namespace ASP.NET.DAL.Repositories;

internal class FolderRepository : IFolderRepository
{
    private readonly AspNetContext _dbContext;

    public FolderRepository(AspNetContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<Folders> GetFolders()
    {
        var allFolders = await _dbContext.Folders.ToListAsync();

        var rootFolders = new List<Entities.Folder>();

        var folderIdToChildFolders = new Dictionary<long, List<Entities.Folder>>();

        foreach (var folder in allFolders)
        {
            if (!folder.ParentFolderId.HasValue)
            {
                rootFolders.Add(folder);
            }
            else
            {
                if (folderIdToChildFolders.ContainsKey(folder.ParentFolderId.Value))
                {
                    folderIdToChildFolders[folder.ParentFolderId.Value].Add(folder);
                }
                else
                {
                    folderIdToChildFolders[folder.ParentFolderId.Value] = new List<Entities.Folder> { folder };
                }
            }
        }

        return new Folders(rootFolders.Select(rootFolder => CreateFolder(rootFolder, folderIdToChildFolders)).ToList());
    }

    private static Folder CreateFolder(Entities.Folder folderEntity, Dictionary<long, List<Entities.Folder>> folderIdToChildFolders)
    {
        if (!folderIdToChildFolders.ContainsKey(folderEntity.Id))
        {
            return new Folder(folderEntity.Id, folderEntity.Title, new List<Folder>());
        }

        var childFolders = folderIdToChildFolders[folderEntity.Id]
            .Select(childFolder => CreateFolder(childFolder, folderIdToChildFolders))
            .ToList();

        return new Folder(folderEntity.Id, folderEntity.Title, childFolders);
    }

    public async Task Save(Folders folders)
    {
        var allFolders = await _dbContext.Folders.ToListAsync();

        _dbContext.Folders.RemoveRange(allFolders);

        await _dbContext.SaveChangesAsync();

        var newFolders = new List<Entities.Folder>();

        foreach (var rootFolder in folders.RootFolders)
        {
            var folder = new Entities.Folder
            {
                Title = rootFolder.Title
            };

            newFolders.Add(folder);

            CrateChildFolders(folder, rootFolder);
        }

        await _dbContext.Folders.AddRangeAsync(newFolders);

        await _dbContext.SaveChangesAsync();

        void CrateChildFolders(Entities.Folder parentFolder, Folder folder)
        {
            foreach (var childFolder in folder.ChildFolders)
            {
                var newFolder = new Entities.Folder
                {
                    Title = childFolder.Title,
                    ParentFolder = parentFolder
                };

                newFolders.Add(newFolder);

                CrateChildFolders(newFolder, childFolder);
            }
        }
    }

    public async Task<IEnumerable<FolderOutputDto>> GetAll()
    {
        var rootFolders = await _dbContext
            .Folders
            .Where(folder => !folder.ParentFolderId.HasValue)
            .ToListAsync();

        var result = new List<FolderOutputDto>();

        foreach (var rootFolder in rootFolders)
        {
            var folder = new FolderOutputDto
            {
                Id = rootFolder.Id,
                Title = rootFolder.Title,
                ChildFolders = await GetChildFolders(rootFolder.Id)
            };

            result.Add(folder);
        }

        return result;
    }

    private async Task<IList<FolderOutputDto>> GetChildFolders(long folderId)
    {
        var childFolders = await _dbContext
            .Folders
            .Where(folder => folder.ParentFolderId == folderId)
            .ToListAsync();

        var result = new List<FolderOutputDto>();

        foreach (var childFolder in childFolders)
        {
            var folder = new FolderOutputDto
            {
                Id = childFolder.Id,
                Title = childFolder.Title,
                ChildFolders = await GetChildFolders(childFolder.Id)
            };

            result.Add(folder);
        }

        return result;
    }
}