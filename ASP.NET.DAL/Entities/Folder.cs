﻿namespace ASP.NET.DAL.Entities;

public class Folder
{
    public long Id { get; set; }

    public string Title { get; set; }

    public long? ParentFolderId { get; set; }

    public virtual Folder? ParentFolder { get; set; }
}