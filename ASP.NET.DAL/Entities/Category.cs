﻿namespace ASP.NET.DAL.Entities;

public class Category
{
    public long Id { get; set; }

    public string Title { get; set; }

    public virtual ICollection<Product> Products { get; set; }
}