﻿using System.ComponentModel.DataAnnotations;

namespace ASP.NET.DAL.Entities;

public class Product
{
    [Required]
    public long Id { get; set; }

    [Required]
    [StringLength(20, MinimumLength = 0)]
    public string Title { get; set; }

    [Required]
    public long CategoryId { get; set; }

    public virtual Category Category { get; set; }

    public DateTime? UpdatedAt { get; set; }

    public bool IsDeleted { get; set; }
}