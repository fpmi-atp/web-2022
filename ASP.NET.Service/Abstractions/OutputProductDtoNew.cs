﻿namespace ASP.NET.Service.Abstractions;

public class OutputProductDtoNew
{
    public long Id { get; }

    public string Title { get; }

    public long CategoryId { get; }

    public string CategoryTitle { get; }

    public OutputProductDtoNew(long id, string title, long categoryId, string categoryTitle)
    {
        // Validation
        Id = id;
        Title = title;
        CategoryId = categoryId;
        CategoryTitle = categoryTitle;
    }
}