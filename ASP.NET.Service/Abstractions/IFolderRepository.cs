﻿namespace ASP.NET.Service.Abstractions;

public interface IFolderRepository
{
    Task<Folders> GetFolders();

    Task Save(Folders folders);
    // Task Add(string title, long? parentFolderId);
    //
    // Task Delete(long folderId);

    Task<IEnumerable<FolderOutputDto>> GetAll();
}