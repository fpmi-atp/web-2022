﻿namespace ASP.NET.Service.Abstractions;

public interface IRepository
{
    Task<IReadOnlyList<OutputProductDtoNew>> GetAll(string? searchString, int? limit, int? offset);

    Task<bool> HasProduct(string title, long categoryId);

    Task<OutputProductDtoNew> CreateProduct(string title, long categoryId);
}