﻿namespace ASP.NET.Service.Abstractions;

public interface IDataStorage
{
    Task<long> Save();
}