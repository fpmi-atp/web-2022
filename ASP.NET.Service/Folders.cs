﻿namespace ASP.NET.Service;

public class Folders
{
    private readonly List<Folder> _rootFolders;

    public IReadOnlyList<Folder> RootFolders => _rootFolders.AsReadOnly();

    public Folders(List<Folder> rootFolders)
    {
        _rootFolders = rootFolders;
    }

    public void AddFolder(string title, long? parentFolderId)
    {
        if (!parentFolderId.HasValue)
        {
            if (_rootFolders.Any(rootFolder => rootFolder.Title == title))
            {
                throw new UserFriendlyException($"Каталог с наименованием {title} уже существует");
            }

            _rootFolders.Add(new Folder(null, title, new List<Folder>()));
        }
        else
        {
            var parentFolder = GetById(parentFolderId.Value);

            parentFolder.AddChild(title);
        }
    }

    private Folder GetById(long folderId)
    {
        foreach (var rootFolder in _rootFolders)
        {
            if (rootFolder.Id == folderId)
            {
                return rootFolder;
            }

            var child = TryFind(rootFolder);

            if (child is not null)
            {
                return child;
            }
        }

        throw new UserFriendlyException($"Каталог с идентификатором {folderId} не найден");

        Folder? TryFind(Folder folder)
        {
            foreach (var childFolder in folder.ChildFolders)
            {
                if (childFolder.Id == folderId)
                {
                    return childFolder;
                }

                var nested = TryFind(childFolder);

                if (nested is not null)
                {
                    return nested;
                }
            }

            return null;
        }
    }

    public bool Remove(long folderId)
    {
        Folder? folderForRemove = null;

        foreach (var rootFolder in _rootFolders)
        {
            if (rootFolder.Id == folderId)
            {
                folderForRemove = rootFolder;
                break;
            }

            if (rootFolder.Remove(folderId))
            {
                return true;
            }
        }

        if (folderForRemove is not null)
        {
            _rootFolders.Remove(folderForRemove);

            return true;
        }

        return false;
    }
}