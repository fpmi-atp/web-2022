﻿using System.ComponentModel.DataAnnotations;

namespace ASP.NET.Service;

public class InputProductDto
{
    [Required]
    [StringLength(200, MinimumLength = 10)]
    public string Title { get; set; }

    public long CategoryId { get; set; }
}