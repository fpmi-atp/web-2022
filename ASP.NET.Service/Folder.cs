﻿namespace ASP.NET.Service;

public class Folder
{
    public long? Id { get; }

    public string Title { get; }

    private readonly List<Folder> _childFolders;

    public IReadOnlyList<Folder> ChildFolders => _childFolders.AsReadOnly();

    public Folder(long? id, string title, IEnumerable<Folder> childFolders)
    {
        Id = id;
        Title = title;
        _childFolders = new List<Folder>(childFolders);
    }

    public void AddChild(string title)
    {
        if (ChildFolders.Any(folder => folder.Title == title))
        {
            throw new UserFriendlyException($"Каталог с наименованием {title} уже существует");
        }

        _childFolders.Add(new Folder(null, title, new List<Folder>()));
    }

    public bool Remove(long folderId)
    {
        Folder? folderForRemove = null;

        foreach (var childFolder in _childFolders)
        {
            if (childFolder.Id == folderId)
            {
                folderForRemove = childFolder;

                break;
            }

            if (childFolder.Remove(folderId))
            {
                return true;
            }
        }

        if (folderForRemove is not null)
        {
            _childFolders.Remove(folderForRemove);

            return true;
        }

        return false;
    }
}