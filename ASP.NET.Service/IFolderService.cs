﻿namespace ASP.NET.Service;

public interface IFolderService
{
    Task Add(string title, long? parentFolderId);

    Task Delete(long folderId);

    Task<IEnumerable<FolderOutputDto>> GetAll();
}