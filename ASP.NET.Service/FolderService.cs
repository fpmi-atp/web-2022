﻿using ASP.NET.Service.Abstractions;

namespace ASP.NET.Service;

internal class FolderService : IFolderService
{
    private readonly IFolderRepository _folderRepository;

    public FolderService(IFolderRepository folderRepository)
    {
        _folderRepository = folderRepository;
    }

    public async Task Add(string title, long? parentFolderId)
    {
        var folders = await _folderRepository.GetFolders();

        folders.AddFolder(title, parentFolderId);

        await _folderRepository.Save(folders);
    }

    public async Task Delete(long folderId)
    {
        var folders = await _folderRepository.GetFolders();

        folders.Remove(folderId);

        await _folderRepository.Save(folders);
    }

    public async Task<IEnumerable<FolderOutputDto>> GetAll()
    {
        return await _folderRepository.GetAll();
    }
}