﻿using System.Runtime.CompilerServices;
using ASP.NET.Service.Abstractions;

[assembly: InternalsVisibleTo("ASP.NET.Service.UnitTests")]
namespace ASP.NET.Service;

internal class ProductService : IProductService
{
    private readonly IRepository _repository;

    public ProductService(IRepository repository)
    {
        _repository = repository;
    }

    public async Task<IEnumerable<OutputProductDtoNew>> Get(
        string? searchString,
        int? limit,
        int? offset,
        bool? orderByAsc,
        CancellationToken cancellationToken)
    {
        return await _repository
            .GetAll(searchString, limit, offset);
    }

    public async Task<OutputProductDto> Get(long productId, CancellationToken cancellationToken)
    {
        throw new Exception();
        // var product = await _dbContext
        //     .Products
        //     .Include(product => product.Category)
        //     .FirstOrDefaultAsync(product => product.Id == productId, cancellationToken);
        //
        // if (product is null)
        // {
        //     throw new Exception("Not found");
        // }
        //
        // return new OutputProductDto
        // {
        //     Id = product.Id,
        //     Title = product.Title,
        //     CategoryId = product.CategoryId,
        //     CategoryTitle = product.Category.Title
        // };
    }

    public async Task<OutputProductDtoNew> Add(
        InputProductDto inputProduct,
        CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(inputProduct.Title))
        {
            throw new UserFriendlyException("Отсутствует название продукта");
        }

        if (inputProduct.Title.Length < 2)
        {
            throw new UserFriendlyException("Слишком короткое название");
        }

        if (inputProduct.Title.Length > 100)
        {
            throw new UserFriendlyException("Слишком длинное название");
        }

        if (await _repository.HasProduct(inputProduct.Title, inputProduct.CategoryId))
        {
            throw new UserFriendlyException(@$"Продукт с названием ""{inputProduct.Title}"" уже существует");
        }

        return await _repository.CreateProduct(inputProduct.Title, inputProduct.CategoryId);
    }

    public async Task<OutputProductDto> Update(
        long productId,
        InputProductDto inputProduct,
        CancellationToken cancellationToken)
    {
        throw new Exception();
        // var productForUpdate = _dbContext.Products.AsQueryable().First(item => item.Id == productId);
        //
        // productForUpdate.Title = inputProduct.Title;
        // productForUpdate.CategoryId = inputProduct.CategoryId;
        //
        // await _dbContext.SaveChangesAsync(cancellationToken);
        //
        // return new OutputProductDto
        // {
        //     Title = productForUpdate.Title,
        //     CategoryId = productForUpdate.CategoryId,
        //     Id = productForUpdate.Id
        // };
    }

    public async Task<OutputProductDto> Delete(
        long productId,
        CancellationToken cancellationToken)
    {
        throw new Exception();
        // var productForDelete = _dbContext.Products.AsQueryable().FirstOrDefault(item => item.Id == productId);
        //
        // if (productForDelete is null)
        // {
        //     throw new Exception();
        // }
        //
        // _dbContext.Products.Remove(productForDelete);
        //
        // await _dbContext.SaveChangesAsync(cancellationToken);
        //
        // return new OutputProductDto
        // {
        //     Title = productForDelete.Title,
        //     CategoryId = productForDelete.CategoryId,
        //     Id = productForDelete.Id
        // };
    }

    public class ProductModel
    {
        public long Id { get; }

        public string Title { get; }

        public long CategoryId { get; }

        public ProductModel(long id, string title, long categoryId)
        {
            Id = id > 0 ? id : throw new Exception();
            if (string.IsNullOrWhiteSpace(title))
            {
                throw new Exception();
            }
            Title = title;
            if (categoryId <= 0)
            {
                throw new Exception();
            }
            CategoryId = categoryId;
        }
    }
}