﻿using Microsoft.Extensions.DependencyInjection;

namespace ASP.NET.Service;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddProductService(this IServiceCollection serviceCollection)
    {
        return serviceCollection
            .AddScoped<IFolderService, FolderService>()
            .AddScoped<IProductService, ProductService>();
    }
}