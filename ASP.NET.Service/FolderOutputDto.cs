﻿namespace ASP.NET.Service;

public class FolderOutputDto
{
    public long Id { get; set; }

    public string Title { get; set; }

    public IList<FolderOutputDto> ChildFolders { get; set; }
}