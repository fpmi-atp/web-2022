﻿namespace ASP.NET.Service;

public class UserFriendlyException : Exception
{
    public UserFriendlyException(string message) : base(message)
    {

    }
}