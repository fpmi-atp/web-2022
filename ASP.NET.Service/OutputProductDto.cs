﻿namespace ASP.NET.Service;

public class OutputProductDto
{
    public long Id { get; set; }

    public string Title { get; set; }

    public long CategoryId { get; set; }

    public string CategoryTitle { get; set; }
}