﻿using ASP.NET.Service.Abstractions;

namespace ASP.NET.Service;

public interface IProductService
{
    public Task<IEnumerable<OutputProductDtoNew>> Get(
        string? searchString,
        int? limit,
        int? offset,
        bool? orderByAsc,
        CancellationToken cancellationToken);

    public Task<OutputProductDto> Get(
        long productId,
        CancellationToken cancellationToken);

    public Task<OutputProductDtoNew> Add(
        InputProductDto inputProduct,
        CancellationToken cancellationToken);

    public Task<OutputProductDto> Update(
        long productId,
        InputProductDto inputProduct,
        CancellationToken cancellationToken);

    public Task<OutputProductDto> Delete(
        long productId,
        CancellationToken cancellationToken);
}