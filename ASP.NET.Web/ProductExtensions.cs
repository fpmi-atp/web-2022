﻿using ASP.NET.DAL.Entities;

namespace ASP.NET.Web;

public static class ProductExtensions
{
    public static string Concatenate(this Product product)
    {
        return $"{product.Id}_{product.Title}";
    }
}