﻿namespace ASP.NET.Web;

public class FirstService
{
    public FirstService(TransientService transientService, ScopedService scopedService, SingletonService singletonService)
    {
        Console.WriteLine("FirstService: ");
        Console.WriteLine($"Transient: {transientService.Guid}");
        Console.WriteLine($"Scoped: {scopedService.Guid}");
        Console.WriteLine($"SingletonService: {singletonService.Guid}");
    }
}