﻿namespace ASP.NET.Web;

public class ScopedService
{
    public Guid Guid;

    public ScopedService()
    {
        Guid = Guid.NewGuid();
        Console.WriteLine($"Created Scoped with GUID: {Guid}");
    }
}