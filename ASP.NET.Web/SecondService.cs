﻿namespace ASP.NET.Web;

public class SecondService
{
    public SecondService(TransientService transientService, ScopedService scopedService, SingletonService singletonService)
    {
        Console.WriteLine("SecondService: ");
        Console.WriteLine($"Transient: {transientService.Guid}");
        Console.WriteLine($"Scoped: {scopedService.Guid}");
        Console.WriteLine($"SingletonService: {singletonService.Guid}");
    }
}