﻿namespace ASP.NET.Web;

public class TransientService
{
    public Guid Guid;

    public TransientService()
    {
        Guid = Guid.NewGuid();
        Console.WriteLine($"Created Transient with GUID: {Guid}");
    }
}