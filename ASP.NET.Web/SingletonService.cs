﻿using Microsoft.Extensions.Options;

namespace ASP.NET.Web;

public class SingletonService
{
    public Guid Guid;

    public SingletonService(IOptions<object> options)
    {
        Guid = Guid.NewGuid();
        Console.WriteLine($"Created Singleton with GUID: {Guid}");
    }
}